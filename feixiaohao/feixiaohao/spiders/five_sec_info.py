# -*- coding: utf-8 -*-
import scrapy


class FiveSecInfoSpider(scrapy.Spider):

    name = 'five_sec_info'
    allowed_domains = ['feixiaohao.com']
    base_url = 'https://www.feixiaohao.com/list_{}.html'
    # start_urls = ['https://www.feixiaohao.com/']

    def start_requests(self):
        for i in range(1, 25):
            yield scrapy.Request(
                url=self.base_url.format(i),
                callback=self.parse_page,
            )

    def parse_page(self, response):
        """提取-每一页数据"""
        print(response.url)

        id_list = response.xpath('//table/tbody/tr/@id').extract()

        price_list = response.xpath('//table/tbody/tr/td[4]/a/text()').extract()

        marketValue_list = response.xpath('//table[@id="table"]/tbody/tr/td[3]/text()').extract()

        for id, price, marketValue in zip(id_list, price_list, marketValue_list):
            item = {}
            # 价格处理--"?", ",", "¥"
            if '¥' in price:
                price = price[1:]
                price = price.replace(',', '')
            elif price == "?":
                price = '0'

            # 流通市值处理: ?->0 + 去掉"¥" + ","的处理  ("亿"单位换算?)
            if '亿' in marketValue:
                # marketValue.replace('¥', '')
                # marketValue.replace('亿', '')
                marketValue = marketValue[1:]
                marketValue = marketValue[:-1]

                if ',' in marketValue:
                    # ¥4,778亿 --> 4,778 --> 4778*10^8
                    marketValue = marketValue.replace(',', '')
                    marketValue = int(marketValue) * 100000000
                else:
                    # ¥0.998亿 --> 0.998 --> 0.998*10^8
                    # marketValue.replace('亿', '')
                    marketValue = float(marketValue) * 100000000

            elif marketValue == '?':
                marketValue = 0
            else:
                # ¥4,778
                marketValue = marketValue[1:]
                marketValue = marketValue.replace(',', '')

            item['id'] = id
            # item['name'] = name
            item['price'] = price
            item['marketValue'] = int(marketValue)

            # print('five_sec_info', rank, '=>', name, "=>", price, "=>", increase)
            yield item


