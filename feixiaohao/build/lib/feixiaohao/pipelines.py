# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import redis


class RedisPipeline(object):
    """Redis-保存管道"""
    def open_spider(self, spider):
        pool = redis.ConnectionPool(host='127.0.0.1', port='6379', db=0, decode_responses=True)
        self.redis_conn = redis.StrictRedis(connection_pool=pool)

    def process_item(self, item, spider):
        """item-数据处理"""
        # redis publish -- 查询上次数据对比,推送变更信息 到 VaryCoinInfo 频道
        last_data = self.redis_conn.hmget(item['id'], ['marketValue', 'price'])

        data = {'marketValue': str(item['marketValue']), 'price': item['price']}
        self.redis_conn.hmset(item['id'], data)

        current_data = list(data.values())

        for value in current_data:
            if value not in last_data:
                msg = {item['id']: data}
                print('last_data -->', item['id'], last_data)
                print("current_data -->", item['id'], current_data)
                print("------VaryCoinInfo------", msg)

                self.redis_conn.publish('VaryCoinInfo', msg)
                break

        return item

# class FeixiaohaoPipeline(object):
#     def process_item(self, item, spider):
#         return item
