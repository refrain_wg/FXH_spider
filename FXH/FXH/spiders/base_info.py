# -*- coding: utf-8 -*-
import json
import os
import scrapy
import time
import urllib
import ssl
ssl._create_default_https_context = ssl._create_unverified_context


class BaseInfoSpider(scrapy.Spider):
    """基本信息爬虫--一次性爬取"""
    name = 'base_info'
    allowed_domains = ['feixiaohao.com']
    start_urls = ['https://www.feixiaohao.com/list_1.html']

    base_url= 'https://www.feixiaohao.com'
    page_url = 'https://www.feixiaohao.com/list_{}.html'
    history_url = 'https://api.feixiaohao.com/coinhisdata/'

    def parse(self, response):
        """提取-总页数"""
        # self.parse_page(response)
        total_pages = response.xpath('//div[@class="new-page-list"]/a[last()-1]/text()').extract_first()

        for i in range(1, int(total_pages)+1):
            yield scrapy.Request(
                url=self.page_url.format(i),
                callback=self.parse_page,
            )

    def parse_page(self, response):
        """解析--每页数据"""
        print(response.url)
        e_list = response.xpath('//table[@id="table"]/tbody/tr')

        for e in e_list:
            id = e.xpath('./@id').extract_first()
            detail_url = e.xpath('./td[2]/a/@href').extract_first()

            yield scrapy.Request(url=self.base_url+detail_url,
                                 callback=self.parse_detail,
                                 meta={'id': id})

    def parse_detail(self, response):
        """解析--详情页"""
        print(response.url)
        id = response.meta["id"]

        try:
            # 提取--小图标,英文名,中文名,相关概念-category,发行时间-timing,白皮书链接-whitePaper,区块站-browser
            logo_url = 'https:' + response.xpath("//div[@class='cell maket']/h1/img/@src").extract_first()
            enName = response.xpath("//div[@class='cell maket']/h1/small/text()").extract_first()
            cnName = response.xpath("//ul/li[2]/span[@class='value']/text()").extract_first()
            timing = response.xpath("//ul/li[4]/span[@class='value']/text()").extract_first()
            whitePaper = response.xpath("//ul/li[5]/span[@class='value']/a/@href").extract_first()
            category = response.xpath('//span[text()="相关概念："]/../span[@class="value"]/a/text()').extract_first()
            # category = response.xpath('//span[text()="相关概念："]/../span[@class="value"]/a/text()').extract()
            website_list = response.xpath("//ul/li[6]/span[@class='value']/a/@href").extract()
            browser_list = response.xpath("//ul/li[7]/span[@class='value']/a/@href").extract()

            enName = enName.replace(' ', '')   # 待验

            if category:
                category = category.strip()

            if not timing or timing == '－':
                timing = None

            if not cnName  or cnName == '－':
                cnName = None

            # whitePaper, website, brower -- 'http:'+'//url'
            if whitePaper != None:
                whitePaper = "https:" + whitePaper
            else:
                whitePaper = None

            if website_list != None:
                website = []
                for url in website_list:
                    web_url = "https:" + url
                    website.append(web_url)
            else:
                website = None

            if browser_list != None:
                browser = []
                for url in browser_list:
                    brower_url = "https:" + url
                    browser.append(brower_url)
            else:
                browser = None

            # 提取 ICO信息 -状态,代币平台,ICO分配,投资者占比,ICO总量,ICO发售量   --22个数据
            ico_elem = response.xpath('//table[@class="iCOtable"]')
            if ico_elem:
                status = ico_elem.xpath('.//tr[2]/td[1]/text()').extract_first()
                plotform = ico_elem.xpath('.//tr[2]/td[2]/text()').extract_first()
                distribution = ico_elem.xpath('.//tr[2]/td[3]/text()').extract_first()
                investor_per = ico_elem.xpath('.//tr[2]/td[4]/text()').extract_first()
                volume = ico_elem.xpath('.//tr[2]/td[5]/text()').extract_first()
                sell = ico_elem.xpath('.//tr[2]/td[6]/text()').extract_first()   # 可能为空

                # 开始时间,结束时间,开售价格,众筹方式,众筹目标,众筹金额
                start_time = ico_elem.xpath('//tr[4]/td[1]/text()').extract_first()
                end_time = ico_elem.xpath('//tr[4]/td[2]/text()').extract_first()
                start_price = ico_elem.xpath('//tr[4]/td[3]/text()').extract_first()
                method = ico_elem.xpath('//tr[4]/td[4]/text()').extract_first()
                target = ico_elem.xpath('//tr[4]/td[5]/text()').extract_first()  # N/A
                money = ico_elem.xpath('//tr[4]/td[6]/text()').extract_first()   # N/A

                # 均价,成功众筹数量,成功众筹金额,特点,安全审计,法律形式
                avg_price = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()
                count = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()    # 可能为空
                success_money = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()  # $?（¥?）
                feature = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()     # N/A
                safe_autid = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()  # N/A
                legal_form = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()  # N/A

                # 管辖区域, 法律顾问, 代售网址, Blog网址
                jurisdiction = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()  # N/A
                counsel = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()       # N/A
                sell_plot = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()  # 空
                blog = ico_elem.xpath('//tr[6]/td[1]/text()').extract_first()       # 空

                keys = ['status', 'plotform', 'distribution', 'investor_per', 'volume', 'sell',
                        'start_time', 'end_time', 'start_price', 'method', 'target',  'money',
                        'avg_price', 'success_money', 'count', 'feature,safe_autid', 'legal_form',
                        'jurisdiction', 'counsel', 'sell_plot', 'blog']
                ico = {}
                for key in keys:
                    ico[key] = eval(key)
                print('ico', '==>', ico)
            else:
                ico = None

            context = {}
            values = ['enName', 'cnName', 'category', 'timing', 'whitePaper', 'website',
                      'browser', 'id']
            for value in values:
                context[value] = eval(value)

            context['ico'] = ico
            # print(context)

            # 下载logo图片-到本地
            LOGO_PATH = "/Users/wanggai/Desktop/FXH_DATA/FXH_LOGO"
            urllib.request.urlretrieve(logo_url, os.path.join(LOGO_PATH, enName + '.png'))

            # 提取简介页面url
            intro_url = self.base_url + response.xpath(
                "//div[@class='cell maket']/div[@class='des']/a/@href").extract_first()

            yield scrapy.Request(url=intro_url,
                                 callback=self.parse_intro,
                                 meta={"context": context})
        except Exception as e:
            print('***** 解析详情页失败 *****', e)

    def parse_intro(self, response):
        """解析--简介详情页"""
        intro = response.xpath("//div[@class='boxContain']/div[@class='artBox']/p[2]/text()").extract_first()
        context = response.meta["context"]

        # print("简介",introduction)
        context['intro'] = intro

        history_url = self.history_url + context['id'] + '/'
        print('history_url', '===>', history_url)
        yield scrapy.Request(url=history_url,
                             callback=self.parse_history_data,
                             meta={'context':context})

    def parse_history_data(self, response):
        """提取--历史价格&历史市值"""
        print(response.url)
        context = response.meta["context"]

        json_data = json.loads(response.text)
        market_list = json_data.get('market_cap_by_available_supply')
        price_list = json_data.get('price_usd')

        market_usd = []
        for market in market_list:
            time_stamp = market[0]/1000
            time_local = time.localtime(time_stamp)
            date_time = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
            # print('时间戳转换---->', time_stamp, time_local, date_time)
            market_data = [date_time, market[1]]
            market_usd.append(market_data)

        price_usd = []
        for price in price_list:
            time_stamp = price[0] / 1000
            time_local = time.localtime(time_stamp)
            date_time = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
            price_data = [date_time, price[1]]
            price_usd.append(price_data)

        context['market_usd'] = market_usd
        context['price_usd'] = price_usd
        print('context', '--->', context)
        yield context

