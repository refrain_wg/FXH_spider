# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Json_Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    id = scrapy.Field()
    enName = scrapy.Field()
    cnName = scrapy.Field()
    category = scrapy.Field()
    timing = scrapy.Field()
    whitePaper = scrapy.Field()
    website = scrapy.Field()
    browser = scrapy.Field()
    introduction = scrapy.Field()
