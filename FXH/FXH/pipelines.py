# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
import os

import pymysql
import redis


class RedisPipeline(object):
    """Redis-保存管道"""
    def open_spider(self, spider):
        if spider.name == 'five_sec_info':
            pool = redis.ConnectionPool(host='127.0.0.1', port='6379', db=0, decode_responses=True)
            self.redis_conn = redis.StrictRedis(connection_pool=pool)

    def process_item(self, item, spider):
        """item-数据处理"""
        if spider.name == 'five_sec_info':
            # redis publish -- 查询上次数据对比,推送变更信息 到 VaryCoinInfo 频道
            last_data = self.redis_conn.hmget(item['id'], ['marketValue', 'price'])

            data = {'marketValue': str(item['marketValue']), 'price': item['price']}
            self.redis_conn.hmset(item['id'], data)

            current_data = list(data.values())

            for value in current_data:
                if value not in last_data:
                    msg = {item['id']: data}
                    print('last_data -->', item['id'], last_data)
                    print("current_data -->", item['id'], current_data)
                    print("------VaryCoinInfo------", msg)

                    self.redis_conn.publish('VaryCoinInfo', msg)
                    break

        return item


class JsonPipeline(object):
    """Json文件--保存管道"""
    def process_item(self, item, spider):
        if spider.name == 'base_info':
            print('item---pipleine', '==>', item)
            enName = item.get('enName')
            dirname = "/Users/wanggai/Desktop/FXH_DATA/FXH_INFO"
            file = os.path.join(dirname, enName + ".json")

            with open(file, "w") as f:
                f.write(json.dumps(item, ensure_ascii=False, indent=2))
            print("保存到json文件--->", file)

        return item

class MySQLPipeline(object):
    """MySQL--保存管道"""
    def open_spider(self, spider):
        if spider.name == 'eve_day_info':
            # 连接MySQL --'127.0.0.1', 3306, use:'coin', pwd:'coin_info', database:'coin_info'
            self.mysql_conn = pymysql.connect(
                host="127.0.0.1",
                port=3306,
                database="coin_info",
                user="root",
                password="Refrain_999999",
                charset='utf8'
            )
            # 获取游标对象
            self.cursor = self.mysql_conn.cursor()

    def process_item(self, item, spider):
        if spider.name == 'eve_day_info':
            try:
                create_tb = '''CREATE TABLE if not exists eve_day_info(
                        id varchar(100) not null ,
                        enName varchar(50) not null,
                        `rank` int not null,
                        marketValue float not null,
                        marketRatio varchar(50) not null,
                        turnVolume float not null,
                        totalVolume float not null,
                        turnRatio varchar(50) not null,
                        turnover float not null,
                        date_time datetime not null,
                        index index_name(id)
                )'''

                # logging.info(create_tb)
                self.cursor.execute(create_tb)
                self.mysql_conn.commit()
            except Exception as e:
                print(e)
                return False

            try:
                data = (item['id'], item['enName'], item['rank'], item['marketValue'],
                        item['marketRatio'],item['turnVolume'],item['totalVolume'],
                        item['turnRatio'], item['turnover'], item["date_time"])
                my_sql = '''insert into eve_day_info(id,enName,`rank`,marketValue,marketRatio,turnVolume,totalVolume,turnRatio,turnover,date_time) values('%s','%s','%d','%f','%s','%f','%f','%s','%f','%s')'''
                # logging.info(my_sql % data)
                self.cursor.execute(my_sql % data)
                print('-------mysql---->', '已保存到数据库中--', data)
            except Exception as e:
                print('保存到mysql中报错:', e)

        return item

    def close_spider(self, spider):
        """关闭爬虫时--断开mysql连接"""
        if spider.name == 'eve_day_info':
            self.cursor.close()
            self.mysql_conn.close()


# class FxhPipeline(object):
#     def process_item(self, item, spider):
#         return item
