# -*- coding: utf-8 -*-
import datetime
import scrapy


class EveDayInfoSpider(scrapy.Spider):
    name = 'eve_day_info'
    allowed_domains = ['feixiaohao.com']
    start_urls = ['http://www.feixiaohao.com/list_1.html']

    base_url = 'https://www.feixiaohao.com'
    page_url = 'https://www.feixiaohao.com/list_{}.html'
    nowTime = datetime.datetime.now().strftime('%Y-%m-%d 0:0:0')

    def parse(self, response):
        """提取-总页数"""
        # self.parse_page(response)
        total_pages = response.xpath('//div[@class="new-page-list"]/a[last()-1]/text()').extract_first()

        for i in range(1, int(total_pages)+1):
            yield scrapy.Request(
                url=self.page_url.format(i),
                callback=self.parse_page,
            )

    def parse_page(self, response):
        """解析--列表页每页数据"""
        print(response.url)
        e_list = response.xpath('//table[@id="table"]/tbody/tr')

        for e in e_list:
            # id, detail_url, 流通市值-marketValue, 成交额-turnover,
            id = e.xpath('./@id').extract_first()
            rank = e.xpath("./td[1]/text()").extract_first()
            detail_url = e.xpath('./td[2]/a/@href').extract_first()
            marketValue = e.xpath("./td[@class='market-cap']/text()").extract_first()
            turnover = e.xpath("./td[6]/a[@class='volume']/text()").extract_first()
            # print('rank', '==>', rank, detail_url)

            # 数据的处理
            # 流通市值处理: ?->0 + 去掉"¥" + ","的处理  + "亿"单位换算?
            if '亿' in marketValue:
                # marketValue.replace('¥', '')
                # marketValue.replace('亿', '')
                marketValue = marketValue[1:]
                marketValue = marketValue[:-1]

                if ',' in marketValue:
                    # ¥4,778亿 --> 4,778 --> 4778*10^8
                    marketValue = marketValue.replace(',', '')
                    marketValue = int(marketValue) * 100000000
                else:
                    # ¥0.998亿 --> 0.998 --> 0.998*10^8
                    marketValue = float(marketValue) * 100000000

            elif marketValue == '?':
                marketValue = 0
            else:
                # ¥4,778
                marketValue = marketValue[1:]
                marketValue = marketValue.replace(',', '')

            # 成交额-数据处理: ?->0 + 去掉"¥" + ","的处理  + "万"单位换算
            if '万' in turnover:
                turnover = turnover[1:]
                turnover = turnover[:-1]
                if ',' in turnover:
                    # ¥4,778万 --> 4,778 --> 4778*10^4
                    turnover = turnover.replace(',', '')
                    turnover = int(turnover) * 10000
                else:
                    # ¥128万 --> 128 --> 128*10^4
                    turnover = int(turnover) * 10000
            elif '亿' in turnover:
                turnover = turnover[1:]
                turnover = turnover[:-1]
                if ',' in turnover:
                    # ¥4,778亿 --> 4,778 --> 4778*10^8
                    turnover = turnover.replace(',', '')
                    turnover = int(turnover) * 100000000
                else:
                    # ¥0.998亿 --> 0.998 --> 0.998*10^8
                    turnover = float(turnover) * 100000000
            elif '?' in turnover:
                turnover = 0
            else:
                # ¥4,778
                turnover = turnover[1:]
                turnover = turnover.replace(',', '')

            context = {'id': id, 'rank': int(rank), 'marketValue': int(marketValue),
                       'turnover': float(turnover)}

            # print('context', '==>', context)
            yield scrapy.Request(url=self.base_url + detail_url,
                                 callback=self.parse_detail,
                                 meta={'context': context})

    def parse_detail(self, response):
        """解析-详情页数据"""
        print(response.url)
        context = response.meta['context']

        enName = response.xpath("//div[@class='cell maket']/h1/small/text()").extract_first()
        turnVolume = response.xpath("//div[@class='cell'][2]/div[@class='value'][1]/text()").extract_first()
        totalVolume = response.xpath("//div[@class='value'][2]/text()").extract_first()
        Ratios = response.xpath("//div/div[@class='chardec']/span/text()").extract()[:2]
        date_time = self.nowTime

        marketRatio = Ratios[0]
        turnRatio = Ratios[1]
        # print("市值比率：%s" % Ratios[0], "流通率：%s" % Ratios[1])

        # 流通量和总发行量--数据处理   21,000,000 BTC --> 21000000
        if '?' in turnVolume:
            turnVolume = 0
        else:
            index = turnVolume.rfind(' ')
            turnVolume = turnVolume[:index]
            turnVolume = turnVolume.replace(',', '')

        if '?' in totalVolume:
            totalVolume = 0
        else:
            total_index = totalVolume.rfind(' ')
            totalVolume = totalVolume[:total_index]
            totalVolume = totalVolume.replace(',', '')

        # 市值比率&流通率: ?的处理
        if '?' in marketRatio:
            marketRatio = 0
        if '?' in turnRatio:
            turnRatio = 0

        # 拼接到 context 中
        context['enName'] = enName
        context['turnVolume'] = float(turnVolume)
        context['totalVolume'] = float(totalVolume)
        context['marketRatio'] = str(marketRatio)
        context['turnRatio'] = str(turnRatio)
        context['date_time'] = date_time

        print('context', '-->', context)
        yield context